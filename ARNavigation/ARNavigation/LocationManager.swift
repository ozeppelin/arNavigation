//
//  LocationManager.swift
//  ARNavigation
//
//  Created by Anderson Silva on 06/07/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationManagerDelegate : class {
    func tracingLocationUser(currentUser:CLLocationCoordinate2D)
    func tracingLocationError(error:Error)
}

class LocationManager : NSObject {
    
    static let sharedInstance : LocationManager = {
        let instance = LocationManager()
        return instance
    }()
    
    private var locationManager : CLLocationManager?
    
    var currentLocation2D : CLLocationCoordinate2D?
    weak var delegate : LocationManagerDelegate?
    
    private override init() {
        super.init()
        locationManager = CLLocationManager()
        
        guard let locationManagers = self.locationManager else { return }
        
        if CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .denied {
            locationManagers.requestAlwaysAuthorization()
            locationManagers.requestWhenInUseAuthorization()
        }
        
        locationManagers.desiredAccuracy = kCLLocationAccuracyBest
        locationManagers.pausesLocationUpdatesAutomatically = false
        locationManagers.startUpdatingLocation()
        locationManagers.delegate = self
        
    }
    
    private func accessConfig() {
        
        do {
            try PreferencesExplorer.open(.locationServices)
        }catch{
            print("TESTE")
        }
        
    }
    
    func isAticveGPS() {
        
        if ((CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied) || (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined)) {
            
        }
        
    }
    
    private func updateLocation(currentLocation:CLLocationCoordinate2D) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocationUser(currentUser: currentLocation)
        
    }
    
    private func updateLocationDidFailWithError(error:Error) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocationError(error: error)
        
    }
    
}
extension LocationManager : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways: locationManager?.startUpdatingLocation(); break;
        case .authorizedWhenInUse: locationManager?.startUpdatingLocation(); break;
        case .notDetermined: locationManager?.requestAlwaysAuthorization(); break;
        case .denied: locationManager?.requestWhenInUseAuthorization(); break;
        default: break;
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locationUser = locations.last else {
            return
        }
        
        self.currentLocation2D = CLLocationCoordinate2D(latitude: locationUser.coordinate.latitude, longitude: locationUser.coordinate.longitude)
        updateLocation(currentLocation: self.currentLocation2D!)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        updateLocationDidFailWithError(error: error)
    }
    
}


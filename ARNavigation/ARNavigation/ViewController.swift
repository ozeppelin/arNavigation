//
//  ViewController.swift
//  ARNavigation
//
//  Created by Anderson Silva on 06/07/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit
import ARCL
import CoreLocation

struct Locais {
    var nome:String!
    var latitude:Double!
    var longitude:Double!
    var img:String!
}

class ViewController: BaseViewController {

    var sceneLocationView = SceneLocationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneLocationView.run()
        view.addSubview(sceneLocationView)
        
        startAR()
    }

    override func viewDidLayoutSubviews() {
        sceneLocationView.frame = view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addObj() -> [Locais] {
        var locais:[Locais] = [Locais]()
        locais.append(Locais(nome: "Rio Mar", latitude: -8.085124, longitude: -34.894727, img: "riomar"))
        locais.append(Locais(nome: "Bode do Nô", latitude: -8.080280, longitude: -34.919403, img: "bodeno"))
        locais.append(Locais(nome: "IMIP", latitude: -8.065876, longitude: -34.890521, img: "imip"))
        locais.append(Locais(nome: "Cesar School", latitude: -8.058860, longitude: -34.872661, img: "cesar"))
        locais.append(Locais(nome: "Hotel Ibis", latitude: -8.094976, longitude: -34.885536, img: "ibis"))
        locais.append(Locais(nome: "Shopping Recife", latitude: -8.117834, longitude: -34.904762, img: "recife"))
        locais.append(Locais(nome: "Accenture - A13", latitude: -8.064524, longitude: -34.871864, img: "accenture"))
        
        
        return locais
    }
    
    func startAR() {
        
        if self.addObj().count > 0 {
            for local in self.addObj() {
                self.startARNavigation(latitude: local.latitude, longitude: local.longitude, img: local.img)
            }
        }
        
    }
    
    func startARNavigation(latitude:Double, longitude:Double, img:String) {

        let latitude:CLLocationDegrees = latitude
        let longitude:CLLocationDegrees = longitude
        let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), altitude: 300)
        let img = UIImage(named: img)
        let annotationNode = LocationAnnotationNode(location: location, image: img!)
//        annotationNode.scaleRelativeToDistance = true
        
        sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: annotationNode)
    }

}
